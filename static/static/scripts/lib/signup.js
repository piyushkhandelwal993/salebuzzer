$(document).ready(function() {
    var sub=true;
    $(".signup-btn").click(function () {
        sub = false;
        $.ajax({
            method: "GET",
            url: "/api/signup",
            success: function (data) {
                console.log(data);
                for (var i in data) {
                    $('#state').append('<option name = state value="' + data[i] + '">' + data[i] + '</option>');
                }
            },

        });
    });
$('#state').change(function () {
    $('#city option:not(:first)').remove();
    var selectedstate = $(this).val();
    $.ajax({
        method: "GET",
        url: "/api/city?state=" + selectedstate,
        success: function (data) {
            for (var i in data) {
                console.log(data);

                $('#city').append('<option  name = city value="' + data[i] + '">' + data[i] + '</option>');
            }
        },

    });
    return false;
});
    $(".btn-registershop").click(function () {
        sub=false;
        $.ajax({
            method: "GET",
            url: "/api/signup",
            success: function (data) {
                console.log(data);
                for (var i in data) {
                    $('#reg-state').append('<option name = state value="' + data[i] + '">' + data[i] + '</option>');
                }
            }
        });
    $.ajax({
        method: "GET",
        url: "/api/domain",
        success: function(data) {
            for(var i in data) {
                $('#firm-domain').append('<option name="firm_domain" value="'+ data[i].id + '">' + data[i].name + '</option>');
            }
        }
    });
    });
    $('#reg-state').change(function () {
        $('#city option:not(:first)').remove();
        var selectedstate = $(this).val();
        $.ajax({
            method: "GET",
            url: "/api/city?state=" + selectedstate,
            success: function (data) {
                for (var i in data) {
                    console.log(data);

                    $('#reg-city').append('<option  name = city value="' + data[i] + '">' + data[i] + '</option>');
                }
            },

        });
        return false;
    });
$('#email').focusout(function () {
    $.post('/api/email', {email: $(this).val()}, function (data, status) {
        console.log(data, status);
        console.log('me chala');
        if (data.flag == '1') {
            $('#email').css({"color": "green"});
            sub=true;
        }
        else {
            alert('Email-Id already exists! Use another Id or Signin with this Id!');
            $('#email').css({"color": "red"});
            sub=false;
        }

    });
});
$('#username').focusout(function () {
    $.post('/api/email', {email: $(this).val()}, function (data, status) {
        console.log(data, status);
        console.log('me chala');
        if (data.flag == '1') {
            $('#emailHelp').text("Email Id is invalid!");
            sub=false;
        }
        else {
            $('#username').css({"color": "green"});
            $('#emailHelp').text(" ");
            
            $('#password').focusout(function () {
                $.post('/api/password', {password: $(this).val(), email: $('#username').val()}, function (data, status) {
                    console.log(data, status);
                    console.log('password chala');
                    if (data.flag == '1') {
                        $('#passwordHelp').text("Password does not match!");
                        sub=false;
                    }
                    else {
                        $('#password').css({"color":"green"});
                        $('#passwordHelp').text(" ");
                        sub=true;
                    }
                });
            });
        }
    });
});
$('#login-form').submit(function(evt){
    console.log('event triggered');
    var credentials={
        "username":$('#username').val(),
        "password":$('#password').val()
    };
    console.log(credentials.username);
    console.log(credentials.password);
    if (sub === true) {
        $.ajax({
            method:"POST",
            url:"/app/login",
            data:credentials,
            success:function(){
                console.log('login successful');
                window.location.reload();
            }
        });
    } else {
        evt.preventDefault();
        alert('Enter correct E-mail or Password');
    }
    return false;
});
$('#cpassword').focusout(function () {
    $.post('/api/confirmpassword',{password:$('#s-password').val(),cpassword: $(this).val()},function (data,status) {
        console.log(data,status);
        console.log('confirm  password chala');
        if (data.flag == '0') {
            $('#cpassword').css({"color":"red"});
            $('#passwordHelp2').text("Password Not Matched!");
            sub=false;
        }
        else {
            $('#cpassword').css({"color":"green"});
            $('#passwordHelp2').text(" ");
            sub=true;
        }
    });
});
    $('#gstn').focusout(function () {
        $.post('/api/gstn', {gstn: $(this).val()}, function (data, status) {
            console.log(data, status);
            console.log('me chala');
            if (data.flag == '1') {
                $('#gstn').css({"color": "green"});
                sub=true;
            }
            else {
                alert('Oops! This GSTN is already registered! You cannot proceed!');
                $('#gstn').css({"color": "red"});
                sub=false;
            }

        });
    });
    $('#mobile_no').focusout(function () {
        $.post('/api/phone', {mobile_no: $(this).val()}, function (data, status) {
            console.log(data, status);
            console.log('me chala bhi');
            if (data.flag == '1'){
                $('#mobile_no').css({"color": "green"});
                sub=true;
            }
            else {
                alert('Mobile No. already exists, please use another mobile!');
                $('#mobile_no').css({"color": "red"});
                sub=false;
            }
        });
    });
    $('#signup-form').submit(function(evt){
        console.log('signup triggered');

        if(sub===false) {
            evt.preventDefault();
            alert('Signup Failed! Please enter correct detatils.');
        } else {
            var credentials = {
                "first_name": $('#first_name').val(),
                "last_name": $('#last_name').val(),
                "email": $('#email').val(),
                "mobile_no": $('#mobile_no').val(),
                // "dob": $('#dob').val(),
                "address": $('#address').val(),
                "city": $('#city').val(),
                "state": $('#state').val(),
                // "pincode":$('#pincode').val(),
                "password": $('#s-password').val()
            };
            console.log(credentials);
            $.ajax({
                method: "POST",
                url: "/app/signup",
                data: credentials,
                success: function () {
                    console.log('signup successful');
                    window.location.reload();
                }
            });
            return false;
        }
    });
    $('#shopregister-form').submit(function(evt){
        console.log('shopregistration triggered');

        if(sub===false) {
            evt.preventDefault();
            alert('Registration Failed! Please enter correct detatils.');
        } else {
            var credentials = {
                "name": $('#name').val(),
                "street": $('#street').val(),
                "locality": $('#a-Address').val(),
                "gstn": $('#gstn').val(),
                "shopNo": $('#shopNo').val(),
                "city": $('#reg-city').val(),
                "state": $('#reg-state').val(),
                "firm_domain": $('#firm-domain').val(),

            };
            console.log(credentials);
            $.ajax({
                method: "POST",
                url: "/app/registerShop",
                data: credentials,
                success: function () {
                    console.log('registerShop successful');
                    window.location.reload();
                }
            });
            return false;
        }
    });

    $('#like').click(function () {
        $.post('/api/likecount', {like : $(this).val()} ,  function(data,status) {
            console.log(data,status);

        });

    });
});
