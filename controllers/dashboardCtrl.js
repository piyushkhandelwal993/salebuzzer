var sessionUtils = require('../utils/sessionUtils');
var util = require('util');
var databaseUtil = require('../utils/databaseUtils');

module.exports = {
  showDashboard: function* (next) {
    var vendorId = this.params.vid;
    var query = util.format('select user_id from vendor_profile where id="%s"', vendorId);
    var user = yield databaseUtil.executeQuery(query);
    if (user[0] !== undefined) {
      query = 'select vendor_profile.*, user.first_name, user.last_name, user.email, user.mobile_no, user.img as dp, firm_domain.name as domain from vendor_profile inner join user on vendor_profile.user_id=user.id inner join firm_domain on firm_domain.id = vendor_profile.domain_id where vendor_profile.id="%s"';
      var merchantDetails = yield databaseUtil.executeQuery(util.format(query, vendorId));
      query = util.format('select id,title,slug,start_date,end_date,event_type_id from event where vendor_id="%s" order by end_date desc', vendorId);
      var contestList = yield databaseUtil.executeQuery(query);
      var participants = [];
      var typeResult = [];
      var showFullDetails = false;
      if ((this.currentUser) && (user[0].user_id == this.currentUser.id)) {
        showFullDetails = true;
        query = 'select id,name from event_type';
        typeResult = yield databaseUtil.executeQuery(query);
      }
      yield this.render('dashboard', {
        merchantDetails: merchantDetails[0],
        contestList: contestList,
        eventTypeList: typeResult,
        vendor_id: vendorId,
        user_id: user[0].user_id,
        showFullDetails: showFullDetails
      });
    } else {
      yield this.render('err',{});
    }
  },
  showallparticipants: function* (next) {
    var vendorId = this.params.vid;
    var query = 'select user_id from vendor_profile where id="%s"';
    var userQuery = util.format(query, vendorId);
    var user = yield databaseUtil.executeQuery(userQuery);
    if (user[0] !== undefined) {
      var user_id = user[0].user_id;
      var showFullDetails = false;
      if ((this.currentUser) && (user_id == this.currentUser.id)) {
        showFullDetails = true;
        query = 'select distinct user.id as id,first_name,last_name,mobile_no,email,title from user inner join user_entity_upload on user_entity_upload.user_id=user.id inner join event on event.id=user_entity_upload.event_id where event.vendor_id="%s"';
      var allparticipants = util.format(query, vendorId);
      var Result1 = yield databaseUtil.executeQuery(allparticipants);
      query = 'select distinct user.id as id,first_name,last_name,mobile_no,email,title from user inner join spinwheel_user_record on spinwheel_user_record.user_id=user.id inner join event on event.id=spinwheel_user_record.event_id where event.vendor_id="%s"';
    allparticipants = util.format(query, vendorId);
    var Result2 = yield databaseUtil.executeQuery(allparticipants);
    for(var i in Result2) {
      Result1.push(Result2[i]);
    }
      query = util.format('select name,img,city from vendor_profile where id="%s"', vendorId);
      var vendor = yield databaseUtil.executeQuery(query);
      query = 'select id,name from event_type';
      var typeResult = yield databaseUtil.executeQuery(query);
      yield this.render('allparticipants', {
        userDetails: Result1,
        merchantDetails: vendor[0],
        showFullDetails: showFullDetails,
        vendor_id:vendorId,
        eventTypeList:typeResult
      });
      } else {
        yield this.render('err',{});
      }
    } else {
      yield this.render('err',{});
    }
  },
  uploadPhoto: function* (next) {
    var vendor_id = this.request.body.fields.id;
    var userUploadedFile = this.request.body.files.image;
    var pic = userUploadedFile.path.split("/");
    var query = util.format('update vendor_profile set img="%s" where id="%s"', pic[3], vendor_id);
    var upload = yield databaseUtil.executeQuery(query);
    this.redirect('/app/dashboard/'+vendor_id);
  }
};