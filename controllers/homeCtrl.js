var sessionUtils = require('../utils/sessionUtils');
var util = require('util');
var databaseUtils = require('./../utils/databaseUtils');

module.exports = {

	showHomePage: function* (next) {

		var queryString = 'select event.id, event_type_id, vendor_id, title, vendor_profile.name, slug, DATEDIFF(end_date, current_timestamp) as datediff, start_date, end_date, total_prizes, description from event join vendor_profile on event.vendor_id= vendor_profile.id where current_timestamp<end_date and current_timestamp>=start_date order by end_date';
		var result1 = yield databaseUtils.executeQuery(queryString);

		queryString = 'select event.id, event_type_id, vendor_id, title, vendor_profile.name, slug,DATEDIFF(start_date, current_timestamp) as datediff, start_date, end_date, total_prizes, description from event join vendor_profile on event.vendor_id=vendor_profile.id where current_timestamp<start_date order by start_date';
		var result2 = yield databaseUtils.executeQuery(queryString);

		queryString = 'select event.id, event_type_id, vendor_id, title, vendor_profile.name, slug, start_date, end_date, total_prizes, description from event join vendor_profile on event.vendor_id=vendor_profile.id where current_timestamp>end_date order by end_date desc';
		var result3 = yield databaseUtils.executeQuery(queryString);

		queryString = 'select substring_index(group_concat(Id order by score desc),",",1) as Id, substring_index(group_concat(img order by score desc),",",1) as img, title, max(score) as points, substring_index(group_concat(first_name order by score desc),",",1) as name from ( select event_id, title, user.id as Id, first_name, count(*) as score, user.img from user_entity_upload inner join user_entity_like on user_entity_upload.id= user_entity_upload_id inner join event on event_id=event.id inner join user on user_entity_upload.user_id=user.id inner join vendor_profile on vendor_profile.id=event.vendor_id where event.end_date<CURRENT_TIMESTAMP group by user_entity_upload_id) as users group by event_id order by event_id desc limit 3';
		var result4 = yield databaseUtils.executeQuery(queryString);
		
		
		
		//var userDetails=result[0];

		yield this.render('home', {
			liveEventList: result1,
			upcomingList: result2,
			archiveList: result3,
			winnerList: result4
		});
	},

	logout: function* (next) {
		var sessionId = this.cookies.get("SESSION_ID");
		if (sessionId) {
			sessionUtils.deleteSession(sessionId);
		}
		this.cookies.set("SESSION_ID", '', { expires: new Date(1), path: '/' });

		this.redirect('/app/home');
	}
};
