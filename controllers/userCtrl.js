var sessionUtils = require('../utils/sessionUtils');
var util = require('util');
var databaseUtils = require('./../utils/databaseUtils');

module.exports = {
    showUserProfile: function* (next) {
        var id = this.params.uid;
        var showFullDetails = false;
        if ((this.currentUser) && (id == this.currentUser.id)) {
            showFullDetails = true;
        }
        var Query = 'select * from user where id="%s"';
        var userNameQuery = util.format(Query, id);
        var result1 = yield databaseUtils.executeQuery(userNameQuery);
        var userDetails = result1[0];
        if (userDetails !== []) {
            Query = 'select distinct event.id,event_type_id,title,slug,start_date,end_date from event inner join user_entity_upload on user_entity_upload.event_id=event.id where user_entity_upload.user_id="%s" order by start_date desc';
            var photoContestHistoryQuery = util.format(Query, id);
            result1 = yield databaseUtils.executeQuery(photoContestHistoryQuery);
            Query = 'select event.id,event_type_id,title,slug,start_date,end_date from event inner join spinwheel_user_record on spinwheel_user_record.event_id=event.id where spinwheel_user_record.user_id="%s" order by start_date desc';
            var spinwheelHistoryQuery = util.format(Query, id);
            var result2 = yield databaseUtils.executeQuery(spinwheelHistoryQuery);
            for (var i in result2) {
                result1.push(result2[i]);
            }
            yield this.render('profile_user', {
                userDetails: userDetails,
                eventList: result1,
                showFullDetails: showFullDetails
            });
        } else {
            yield this.render('err',{});
          }
    },
    login: function* (next) {
        var username = this.request.body.username;
        var password = this.request.body.password;
        var error;
        var query = util.format('select * from user where email="%s" and password="%s"', username, password);
        var result = yield databaseUtils.executeQuery(query);
        var user = result[0];
        if (result.length > 0) {
            query = util.format('select id as vid from vendor_profile where user_id="%s"', user.id);
            var result2 = yield databaseUtils.executeQuery(query);
            var vid = [];
            for (var i = 0; i < result2.length; i++) {
                vid.push(result2[i].vid);
            }
            if (vid.length > 0)
                user.vid = vid;
            sessionUtils.saveUserInSession(user, this.cookies);
            this.redirect('/app/home');
        } else {
            yield this.render('err',{});
          }
    },
    signup: function* (next) {
        var first_name = this.request.body.first_name;
        var last_name = this.request.body.last_name;
        var email = this.request.body.email;
        var mobile_no = this.request.body.mobile_no;
        var city = this.request.body.city;
        var locality = this.request.body.address;
        var state = this.request.body.state;
        var password = this.request.body.password;
        var queryString = 'insert into user(first_name,last_name,email,mobile_no,city,locality,state,password,gender) values("%s","%s","%s","%s","%s","%s","%s","%s","%s")';
        var query = util.format(queryString, first_name, last_name, email, mobile_no, city, locality, state, password,' ');

        var errorMessage;

        try {
            var result = yield databaseUtils.executeQuery(query);
            queryString = 'select * from user where id=%s';
            query = util.format(queryString, result.insertId);
            result = yield databaseUtils.executeQuery(query);
            var insertedGuest = result[0];
            
            sessionUtils.saveUserInSession(insertedGuest, this.cookies);
        } catch (e) {
            if (e.code === 'ER_DUP_ENTRY') {
                errorMessage = 'Guest already exists';
            }
            else {
                throw e;
            }
        }
        if (errorMessage) {
            yield this.render('err',{});
        }
        else {
            this.redirect('/app/home');
        }
    },
    registerShop: function* (next) {

        qs = 'insert into vendor_profile(name,domain_id,user_id,gstn,shopNo,street,locality,city,state) values ("%s","%s","%s","%s","%s","%s","%s","%s","%s")';
        query = util.format(qs, this.request.body.name, this.request.body.firm_domain, this.currentUser.id, this.request.body.gstn, this.request.body.shopNo, this.request.body.street, this.request.body.locality, this.request.body.city,this.request.body.state);
        result = yield databaseUtils.executeQuery(query);

        if(this.currentUser.vid)
            this.currentUser.vid.push(result.insertId);
        else
            this.currentUser.vid=[result.insertId];

        sessionUtils.saveUserInSession(this.currentUser, this.cookies);

        this.redirect('/app/dashboard/'+result.insertId);
    },
    uploadPhoto: function* (next) {
        var user_id = this.request.body.fields.id;
        var pic = userUploadedFile.path.split("/");
        var query = util.format('update user set img="%s" where id="%s"', pic[3], user_id);
        var upload = yield databaseUtils.executeQuery(query);
        this.redirect('/app/profile/'+user_id);
    }

};