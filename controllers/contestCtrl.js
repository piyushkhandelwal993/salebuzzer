var sessionUtils = require('../utils/sessionUtils');
var util = require('util');
var databaseUtils = require('./../utils/databaseUtils');
var mailUtils=require('./../utils/mailUtils');

module.exports = {
  showContestDetailPage: function* (next) {
    var eventId = this.params.eid;
    var queryString = 'select event_type_id,slug, event.id as eid, title, vendor_profile.name, vendor_profile.id as id,vendor_profile.user_id as vuid, description, prize_description, start_date, end_date, total_prizes, prize_validity from event inner join vendor_profile on event.vendor_id=vendor_profile.id where event.id="%s" and event_type_id=1';
    var query = util.format(queryString, eventId);
    var contestDetails = yield databaseUtils.executeQuery(query);
    
    queryString = 'select count(user_entity_upload.id) as no_of_submissions, count(distinct user_id) as no_of_users from user_entity_upload where event_id="%s"';
    query = util.format(queryString, eventId);
    var userNumber = yield databaseUtils.executeQuery(query);

    queryString='select user.id as UID,user.first_name,user.last_name,user_entity_upload.id as PhotoID,entity_file_url from user join user_entity_upload on user_entity_upload.user_id=user.id where event_id="%s" and verified=false';
    query=util.format(queryString,eventId);
    var unapprovedPhotos=yield databaseUtils.executeQuery(query);

    queryString='select user.id as UID,user.first_name,user.last_name, user_entity_upload.id as PhotoID,entity_file_url,count(*) - 1 as Likes from user join user_entity_upload on user_entity_upload.user_id=user.id join user_entity_like on user_entity_like.user_entity_upload_id=user_entity_upload.id where event_id="%s" group by PhotoID order by count(*) desc';
    query=util.format(queryString,eventId);
    var approvedPhotos=yield databaseUtils.executeQuery(query);

    var showFullDetails = false;
    if ((this.currentUser) && (contestDetails[0].vuid == this.currentUser.id)) {
      showFullDetails = true;
    }
    yield this.render('contest', {
      contestDetails: contestDetails[0],
      unapprovedPhotos:unapprovedPhotos,
      approvedPhotos:approvedPhotos,
      userNumber: userNumber[0],
      eid: eventId,
      showFullDetails: showFullDetails
    });
  },
  showContestsPage: function* (next) {
    var status = this.params.status;
    var qs = "";
    if (status === 'live') {
      qs = 'select event.id,event_type_id, title, vendor_profile.name, vendor_profile.id as vid, slug, start_date, end_date, total_prizes from event join vendor_profile on event.vendor_id=vendor_profile.id where current_timestamp < end_date and current_timestamp >= start_date order by end_date';
    }
    else if (status === 'upcoming') {
      qs = 'select event.id,event_type_id, title, vendor_profile.name, vendor_profile.id as vid, slug, start_date, end_date, total_prizes from event join vendor_profile on event.vendor_id=vendor_profile.id where current_timestamp<start_date order by start_date';
    }
    var result = yield databaseUtils.executeQuery(qs);
    yield this.render('contest_list', {
      contest_list: result,
    });
  },
  contestuplode: function* (next) {
    var id = '/app/slug/contest/' + this.request.body.fields.eid;
    var eventId = this.request.body.fields.eid;
    var cu = this.currentUser;
    var userUploadedFile = this.request.body.files;
    var pic = userUploadedFile.userUploadedFile.path.split('/');
    var entity_type_id = 1;//for photocontest
    var qurery = yield databaseUtils.executeQuery(util.format('insert into user_entity_upload(entity_type_id,user_id,event_id,entity_file_url) values("%s","%s","%s","%s")', entity_type_id, cu.id, parseInt(eventId), pic[3]));
    this.redirect(id);
  },
  likeCount: function* (next) {
    var id = '/app/slug/contest/' + this.request.body.eid;
    var like = this.request.body.like;
    var r = yield databaseUtils.executeQuery(util.format('SELECT * FROM user_entity_like WHERE user_entity_upload_id=%s and USER_ID=%s', parseInt(like), parseInt(this.currentUser.id)));
    if (r.length == 0) {

      r = yield databaseUtils.executeQuery(util.format('INSERT INTO user_entity_like (user_entity_upload_id,USER_ID) VALUES(%s,%s)', parseInt(like), parseInt(this.currentUser.id)));
    }
    else {
      r = yield databaseUtils.executeQuery(util.format('DELETE FROM user_entity_like  WHERE user_entity_upload_id=%s and USER_ID=%s', parseInt(like), parseInt(this.currentUser.id)));

    }
    this.redirect(id);
  },
  showNewContestForm: function* (next) {
    var vendor_id = this.request.body.vendor_id;
    var event_type = this.request.body.event_type;
    var qs = util.format('select id,name from event_type where id="%s"', event_type);
    var result = yield databaseUtils.executeQuery(qs);
    var type = result[0];
    if (type.name === 'photo Event') {
      yield this.render('photoContestForm', {
        event_type_id: type.id,
        vendor_id: vendor_id
      });
    } else if (type.name === 'spinwheel') {
      yield this.render('wheel_form', {
        event_type_id: type.id,
        vendor_id: vendor_id
      });
    }
  },
  addNewPhotoContest: function* (next) {
    var moderator_required = this.request.body.moderator_required ? 1 : 0;
    var slug = this.request.body.title + "-" + this.request.body.vendor_id + "-" + this.request.body.event_type_id;
    var qs = 'insert into event(title,event_type_id,vendor_id,start_date,end_date,total_prizes,moderator_required,description,prize_description,prize_validity,slug) values("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")';
    var query = util.format(qs, this.request.body.title, this.request.body.event_type_id, this.request.body.vendor_id, this.request.body.start_date, this.request.body.end_date, this.request.body.total_prizes, moderator_required, this.request.body.description, this.request.body.prize_description, this.request.body.prize_validity, slug);
    var result = yield databaseUtils.executeQuery(query);
    qs = 'select id from event where slug="%s"';
    query = util.format(qs, slug);
    var c_id = yield databaseUtils.executeQuery(query);
   this.redirect('/app/'+slug+'/contest/'+c_id[0].id);
  },
  addNewSpinWheel: function* (next) {
    var slug = this.request.body.title + "-" + this.request.body.vendor_id + "-" + "spinwheel";
    var event_type_id = this.request.body.event_type_id;

    var qs = 'insert into event(title,Event_type_id,vendor_id,start_date,end_date,total_prizes,description,prize_validity,slug,prize_description) values("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")';

    var query = util.format(qs, this.request.body.title,event_type_id, this.request.body.vendor_id, this.request.body.start_date, this.request.body.end_date, this.request.body.sectors, this.request.body.description, this.request.body.prize_validity, slug,this.request.body.total_prizes);

    var result = yield databaseUtils.executeQuery(query);
    var event_id = result.insertId;

    qs = 'insert into spinwheel_offer (event_id, sector_data) values ("%s","%s")';
    for (var i = 0; i < this.request.body.offer.length; i++) {
      query = util.format(qs, event_id, this.request.body.offer[i]);
      result = yield databaseUtils.executeQuery(query);
    }
    var redirectUrl = '/app/' + slug + '/spinwheel/' + event_id;
    this.redirect(redirectUrl);
  },
  closeContest: function* (next) {
    var slug = this.params.slug;
    var eventId = this.params.eid;
    var queryString = 'update event set end_date=now() where id="%s"';
    var query = util.format(queryString, eventId);
    var result = yield databaseUtils.executeQuery(query);

    queryString='select event_type_id from event where event.id="%s"';
    query= util.format(queryString, eventId);
    var event_type_id= yield databaseUtils.executeQuery(query);
    yield this.render('ended', {
      event_id:eventId,
      event_type_id:event_type_id[0],
      slug:slug
    });

  },
  verifyPhoto: function*(next){
    var user_entity_upload_id=this.request.body.user_entity_upload_id;
    var eid=this.request.body.eid;
    var query=util.format('update user_entity_upload set verified=true where id="%s"',user_entity_upload_id);
    var result=yield databaseUtils.executeQuery(query);
    query=util.format('insert into user_entity_like (user_entity_upload_id,user_id) values ("%s",%d)',user_entity_upload_id,20);
    result=yield databaseUtils.executeQuery(query);
    this.redirect('/app/slug/contest/'+eid);
  },
  createCoupons: function*(next){
    var eventId=this.request.body.eid;
    var total_prizes=this.request.body.total_prizes;
    var queryString='select user.first_name,user.last_name,user.email, event.title,event.prize_validity,vendor_profile.name, user_entity_upload_id,count(*) as Likes from user_entity_like join user_entity_upload on user_entity_like.user_entity_upload_id=user_entity_upload.id join event on event.id=user_entity_upload.event_id join vendor_profile on vendor_profile.id=event.vendor_id join user on user.id=user_entity_upload.user_id where event.id="%s" group by user_entity_upload_id order by count(*) desc limit %s';
    var query=util.format(queryString,eventId,total_prizes);
    var result=yield databaseUtils.executeQuery(query);
    var msgForVendor="";
    var mailSubject='Promo Code';
    for(var i in result){
      var coupon=(new Date()).getUTCMilliseconds()+'-'+result[0].title.slice(0,4)+'-'+i;
      var msg='<h3>Congratulations!</h3><h2>Your Coupon code is: '+coupon+'</h2>';
      msg+='Thank you for participating in <b>'+result[0].title+'</b><br />';
      msg+=' You can redeem your coupon at <b>'+result[0].name+'</b><br />';
      msg+=' Visit promozonic.projectorclub.in for more exciting offers';
      mailUtils.sendMail(result[i].email,mailSubject,msg,'html');
      msgForVendor+= result[i].first_name+' '+result[i].last_name+' '+result[i].email+' '+coupon+'\n';
    }
    mailUtils.sendMail(this.currentUser.email,'Winners List',msgForVendor);
    this.body=msgForVendor;
  }
};
