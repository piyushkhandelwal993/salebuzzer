var sessionUtils = require('../utils/sessionUtils');
var util= require('util');
var databaseUtils= require('./../utils/databaseUtils');
var mailUtils=require('./../utils/mailUtils');

module.exports = {
    showSpinWheel: function* (next) {
        var qs = 'select sector_data from spinwheel_offer where event_id="%s"';
        var query=util.format(qs,this.params.cid);
        var spin =  yield databaseUtils.executeQuery(query);
        qs='select event_type_id,slug,vendor_profile.user_id as vuid, event.id as cid, title, vendor_profile.name, vendor_profile.id as id, description, prize_description, start_date, end_date, total_prizes, prize_validity from event inner join vendor_profile on event.vendor_id=vendor_profile.id where event.id="%s" and event_type_id=6';
        query=util.format(qs,this.params.cid);
        var contestDetails=yield databaseUtils.executeQuery(query);
        qs='select count(*) as count from spinwheel_user_record where event_id="%s"';
        query=util.format(qs,this.params.cid);
        var participants= yield databaseUtils.executeQuery(query);
        qs = 'select * from spinwheel_user_record  where user_id="%s" and event_id="%s"';
        var hasPlayed=[];
        if(this.currentUser){
            query=util.format(qs,this.currentUser.id,this.params.cid);
            hasPlayed=yield databaseUtils.executeQuery(query);
        }
        var showFullDetails = false;
        if ((this.currentUser) && (contestDetails[0].vuid == this.currentUser.id)) 
        {
             showFullDetails = true;
        }
	    yield this.render('wheel',{
            hasPlayed:hasPlayed,
            participants:participants,
            spinWheel:spin,
            contestDetails:contestDetails,
            showFullDetails:showFullDetails
        });
    },
   
    insertWheeldata: function* (next) {
        var eid=this.request.body.eid;
       var uid=this.request.body.uid;
       var vid=this.request.body.vid;
       var sdata=this.request.body.sector_data;
       var couponcode=this.request.body.coupon;
       var qs='insert into spinwheel_user_record (user_id,vendor_id,event_id,spinwheel_offer,coupon_code) values ("%s","%s","%s","%s","%s")';
       var query=util.format(qs,uid,vid,eid,sdata,couponcode);
       var result=yield databaseUtils.executeQuery(query);
       query=util.format('select vendor_profile.name,event.title from vendor_profile join event on event.vendor_id=vendor_profile.id where event.id="%s" and vendor_profile.id="%s"',eid,vid);
       result=yield databaseUtils.executeQuery(query);
       if(sdata!=='Better luck next time'){
        var subject='Promo Code'
         var msg='<h3>Congratulations!</h3><h2>Your Coupon code is: '+couponcode+'</h2>'
         msg+='<h4>You have won '+sdata+'</h4>'
         msg+='Thank you for participating in <b>'+result[0].title+'</b><br />'
         msg+=' You can redeem your coupon at <b>'+result[0].name+'</b><br />'
         msg+=' Visit promozonic.projectorclub.in for more exciting offers'
         mailUtils.sendMail(this.currentUser.email,subject,msg,'html')
    }  
    }
    
};