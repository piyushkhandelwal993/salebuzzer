var sessionUtils = require('../utils/sessionUtils');
var util= require('util');
var databaseUtils= require('./../utils/databaseUtils');
var data=require('./../controllers/state').state;
module.exports = {
    getdomain: function* (next) {
        var domainList = yield databaseUtils.executeQuery(util.format('select id,name from firm_domain'));
        this.body = domainList;
    },
    getstate: function* (next) {
        var state=[];
        for(var i in data){
            state.push(i);
        }
        
        state.sort();
        this.body=state;
    },
    getcity:function* (next) {
      var city=[];
      var selectedstate=this.request.query.state;
      
      this.body=data[selectedstate];
    },
    checkemail:function* (next) {
        var email =this.request.body.email;

        var query=yield databaseUtils.executeQuery(util.format('select * from user where email="%s"',email));
        if(query.length==0){
            this.body={flag:'1'}
        }
        else this.body={flag:'0'}
    },
    checkphone:function* (next) {
        var mobile_no =this.request.body.mobile_no;
        var query=yield databaseUtils.executeQuery(util.format('select * from user where mobile_no=%s',mobile_no));
        

        if (query.length==0 && mobile_no.length==10) {
            this.body = {flag: '1'}
        }else  this.body={flag:'0'}
    },
    checkpassword:function* (next) {
        var password =this.request.body.password;
        var email = this.request.body.email;
        var querypassword=yield databaseUtils.executeQuery(util.format('select password from user where email="%s"',email));
        if(password===querypassword[0].password){
            this.body={flag:'0'};
        }
        else this.body={flag:'1'};
    },
    checkconfirmpassword: function* (next) {
        var password = this.request.body.password;
        var cpassword = this.request.body.cpassword;
        if (password === cpassword) {
            this.body = {flag: '1'};
        }
        else this.body = {flag: '0'};
    },
    checkphone:function* (next) {
        var mobile_no =this.request.body.mobile_no;
        var query=yield databaseUtils.executeQuery(util.format('select * from user where mobile_no=%s',mobile_no));
        if(mobile_no.length== 10 && query.length==0){
            this.body={flag:'1'}
        }else this.body={flag:'0'};

    },
    checkgstn:function* (next) {
        var gstn =this.request.body.gstn;
        var query=yield databaseUtils.executeQuery(util.format('select * from vendor_profile where gstn="%s"',gstn));
        if(query.length==0){
            this.body={flag:'1'}
        }
        else this.body={flag:'0'}
    },

    likeCount: function* (next) {
        var flag=0;
        var like = this.request.body.like;
        var r = yield databaseUtils.executeQuery(util.format('SELECT * FROM user_entity_like WHERE user_entity_upload_id=%s and USER_ID=%s', parseInt(like), parseInt(this.currentUser.id)));
        if (r.length == 0) {
            flag=1;
            r = yield databaseUtils.executeQuery(util.format('INSERT INTO user_entity_like (user_entity_upload_id,USER_ID) VALUES(%s,%s)', parseInt(like), parseInt(this.currentUser.id)));
        }
        else {
            r = yield databaseUtils.executeQuery(util.format('DELETE FROM user_entity_like  WHERE user_entity_upload_id=%s and USER_ID=%s', parseInt(like), parseInt(this.currentUser.id)));
        }
        this.body={flag:flag}
    },
};