-- MySQL dump 10.11
--
-- Host: localhost    Database: promozonic
-- ------------------------------------------------------
-- Server version	5.0.45-community-nt

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `entity_type`
--

DROP TABLE IF EXISTS `entity_type`;
CREATE TABLE `entity_type` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `creation_timestamp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `name` varchar(15) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `entity_type`
--

LOCK TABLES `entity_type` WRITE;
/*!40000 ALTER TABLE `entity_type` DISABLE KEYS */;
INSERT INTO `entity_type` VALUES (1,'2018-06-19 10:22:45','image'),(2,'2018-06-19 10:22:45','video'),(4,'2018-06-19 10:22:45','audio');
/*!40000 ALTER TABLE `entity_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event`
--

DROP TABLE IF EXISTS `event`;
CREATE TABLE `event` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `creation_timestamp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `title` varchar(255) NOT NULL,
  `Event_type_id` int(11) unsigned default NULL,
  `vendor_id` int(11) unsigned default NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `total_prizes` int(11) unsigned NOT NULL,
  `Moderator_required` tinyint(1) default NULL,
  `description` text,
  `prize_description` text,
  `prize_validity` date default NULL,
  `slug` varchar(127) default NULL,
  `winner_id` int(11) unsigned default NULL,
  PRIMARY KEY  (`id`),
  KEY `fk5` (`Event_type_id`),
  KEY `fk6` (`vendor_id`),
  KEY `fk_prize` (`winner_id`),
  CONSTRAINT `fk5` FOREIGN KEY (`Event_type_id`) REFERENCES `event_type` (`id`),
  CONSTRAINT `fk6` FOREIGN KEY (`vendor_id`) REFERENCES `vendor_profile` (`id`),
  CONSTRAINT `fk_prize` FOREIGN KEY (`winner_id`) REFERENCES `prize` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `event`
--

LOCK TABLES `event` WRITE;
/*!40000 ALTER TABLE `event` DISABLE KEYS */;
INSERT INTO `event` VALUES (1,'2018-07-21 23:19:07','City Under Rain',1,1,'2018-07-22','2018-08-18',150000,1,'Click it, Upload it , win it.','Chance to win a trip to the London','2018-08-31','City Under Rain-1-1',NULL),(2,'2018-07-21 23:26:53','Spin It Fast ',6,2,'2018-07-22','2018-08-22',6,NULL,'Spin it, Win it.','150000','2018-09-02','Spin It Fast -2-spinwheel',NULL),(3,'2018-07-24 06:56:19','Food Love',1,3,'2018-07-22','2018-08-01',200000,1,'Click your pics at McDonald\'s and upload it to win exciting prizes','Chance to get 100% off for a meal','2018-08-23','Food Love-3-1',NULL),(4,'2018-07-23 05:09:48','Tu Kheech Meri Photo',1,4,'2018-07-23','2018-07-23',300000,1,'Your photo should be a selfie in which electronic gadgets are used.','1st Prize: MacBook Pro\r\n2nd Prize : iPhone 6\r\n3rd Prize: 2TB HardDisk','2018-08-01','Tu Kheech Meri Photo-4-1',NULL),(5,'2018-07-23 05:14:35','Bag The Wheel',6,4,'2018-07-25','2018-07-28',7,NULL,'Spin the wheel. Get exciting offers.','150000','2018-07-30','Bag The Wheel-4-spinwheel',NULL),(6,'2018-07-24 05:45:59','Just Testing',1,2,'2018-07-24','2018-07-24',105000,1,'fhdfhsdkjfh','1st prize: iPhone X','2018-07-26','Just Testing-2-1',NULL);
/*!40000 ALTER TABLE `event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_type`
--

DROP TABLE IF EXISTS `event_type`;
CREATE TABLE `event_type` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `creation_timestamp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `name` varchar(63) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `event_type`
--

LOCK TABLES `event_type` WRITE;
/*!40000 ALTER TABLE `event_type` DISABLE KEYS */;
INSERT INTO `event_type` VALUES (1,'2018-06-19 10:19:52','photo Event'),(6,'2018-06-29 09:55:49','spinwheel');
/*!40000 ALTER TABLE `event_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `firm_domain`
--

DROP TABLE IF EXISTS `firm_domain`;
CREATE TABLE `firm_domain` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `creation_timestamp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `name` varchar(63) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `firm_domain`
--

LOCK TABLES `firm_domain` WRITE;
/*!40000 ALTER TABLE `firm_domain` DISABLE KEYS */;
INSERT INTO `firm_domain` VALUES (1,'2018-06-21 01:02:39','garments'),(2,'2018-06-21 01:02:50','electronics'),(3,'2018-06-21 01:03:09','cosmetics'),(4,'2018-06-21 01:03:24','movies'),(5,'2018-06-21 01:03:34','groceries'),(6,'2018-07-04 08:25:30','others'),(7,'2018-07-19 04:55:30','books and stationery'),(8,'2018-07-19 04:55:44','automobiles'),(9,'2018-07-19 04:56:05','bags and luggage'),(10,'2018-07-19 04:56:48','fitness'),(11,'2018-07-19 04:57:03','kids'),(12,'2018-07-19 04:58:34','medicine');
/*!40000 ALTER TABLE `firm_domain` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prize`
--

DROP TABLE IF EXISTS `prize`;
CREATE TABLE `prize` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `creation_timestamp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `event_id` int(11) unsigned default NULL,
  `user_id` int(11) unsigned default NULL,
  `vendor_id` int(11) unsigned default NULL,
  `coupon_code` varchar(15) default NULL,
  `recieved` tinyint(1) default NULL,
  `score` int(11) default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `coupon_code` (`coupon_code`),
  KEY `fk7` (`vendor_id`),
  KEY `fk8` (`user_id`),
  KEY `fk9` (`event_id`),
  CONSTRAINT `fk7` FOREIGN KEY (`vendor_id`) REFERENCES `vendor_profile` (`user_id`),
  CONSTRAINT `fk8` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `fk9` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `prize`
--

LOCK TABLES `prize` WRITE;
/*!40000 ALTER TABLE `prize` DISABLE KEYS */;
INSERT INTO `prize` VALUES (1,'2018-06-21 02:31:11',1,1,1,'XDS@879',1,0),(2,'2018-06-21 02:31:11',2,2,2,'GDS@879',1,0),(3,'2018-06-21 02:31:11',3,3,3,'HGS@879',1,0),(4,'2018-06-21 02:31:11',4,4,4,'YTS@879',0,0),(5,'2018-06-21 05:44:49',5,3,2,'pqr@987',0,0),(6,'2018-06-21 02:31:11',1,6,1,'sdr@987',0,0),(7,'2018-06-21 02:31:11',1,7,1,'bnh@333',0,0),(8,'2018-06-21 03:31:27',2,7,2,'jkh@333',0,0),(9,'2018-06-21 03:31:27',2,8,2,'jkh@993',0,0),(10,'2018-06-21 03:31:27',3,9,3,'yui@393',0,0),(11,'2018-06-21 03:31:27',3,10,3,'dui@393',0,0),(12,'2018-06-21 03:31:27',4,10,4,'fdi@393',0,0),(13,'2018-06-21 03:31:27',4,5,4,'mji@393',0,0),(14,'2018-06-21 05:44:49',5,3,2,'fvi@393',0,0);
/*!40000 ALTER TABLE `prize` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `creation_timestamp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `name` varchar(15) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'2018-06-21 00:52:55','end_user'),(2,'2018-06-21 00:53:25','vendor'),(3,'2018-06-21 00:53:34','admin');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spinwheel_offer`
--

DROP TABLE IF EXISTS `spinwheel_offer`;
CREATE TABLE `spinwheel_offer` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `creation_timestamp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `event_id` int(10) unsigned default NULL,
  `sector_data` varchar(63) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `fk_spin` (`event_id`),
  CONSTRAINT `fk_spin` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `spinwheel_offer`
--

LOCK TABLES `spinwheel_offer` WRITE;
/*!40000 ALTER TABLE `spinwheel_offer` DISABLE KEYS */;
INSERT INTO `spinwheel_offer` VALUES (1,'2018-07-21 23:26:53',2,'10% off'),(2,'2018-07-21 23:26:54',2,'15% off'),(3,'2018-07-21 23:26:54',2,'21% off'),(4,'2018-07-21 23:26:54',2,'55% off'),(5,'2018-07-21 23:26:54',2,'41% off'),(6,'2018-07-21 23:26:54',2,'61% off'),(7,'2018-07-23 05:14:35',5,'5% off'),(8,'2018-07-23 05:14:35',5,'10% off'),(9,'2018-07-23 05:14:35',5,'20% off'),(10,'2018-07-23 05:14:35',5,'40% off'),(11,'2018-07-23 05:14:36',5,'30% off'),(12,'2018-07-23 05:14:36',5,'70% off'),(13,'2018-07-23 05:14:36',5,'15% off');
/*!40000 ALTER TABLE `spinwheel_offer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spinwheel_user_record`
--

DROP TABLE IF EXISTS `spinwheel_user_record`;
CREATE TABLE `spinwheel_user_record` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `creation_timestamp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `user_id` int(10) unsigned default NULL,
  `vendor_id` int(10) unsigned default NULL,
  `event_id` int(10) unsigned default NULL,
  `spinwheel_offer` varchar(127) default NULL,
  `coupon_code` varchar(127) default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `user_id` (`user_id`,`event_id`),
  KEY `fksw2` (`vendor_id`),
  KEY `fksw3` (`event_id`),
  CONSTRAINT `fksw1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `fksw2` FOREIGN KEY (`vendor_id`) REFERENCES `vendor_profile` (`id`),
  CONSTRAINT `fksw3` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `spinwheel_user_record`
--

LOCK TABLES `spinwheel_user_record` WRITE;
/*!40000 ALTER TABLE `spinwheel_user_record` DISABLE KEYS */;
/*!40000 ALTER TABLE `spinwheel_user_record` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `creation_timestamp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `first_name` varchar(15) NOT NULL,
  `last_name` varchar(15) NOT NULL,
  `email` varchar(63) NOT NULL,
  `mobile_no` varchar(15) NOT NULL,
  `dob` date default NULL,
  `gender` char(1) NOT NULL,
  `house_no` varchar(15) default NULL,
  `street` varchar(63) default NULL,
  `city` varchar(63) NOT NULL,
  `locality` varchar(511) NOT NULL,
  `state` varchar(63) default NULL,
  `pincode` varchar(6) default NULL,
  `active` tinyint(1) default NULL,
  `img` varchar(255) default 'user.png',
  `password` varchar(127) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `unique_email` (`email`),
  UNIQUE KEY `unique_mobile` (`mobile_no`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (20,'2018-07-21 11:17:58','Jarvis','Stark','promozonic@gmail.com','07007258711','2018-07-21','',NULL,NULL,'North and Middle Andaman','h.no.206,mo. mahmand jalal nagar behind','Andaman and Nicobar Island (UT)',NULL,NULL,'user.png','promozonic@123'),(21,'2018-07-21 23:12:06','RAHUL','DIXIT','rahul.dixit_cs16@gla.ac.in','8953879430',NULL,'',NULL,NULL,'Shahjahanpur','h.no.206,mo. mahmand jalal nagar','Uttar Pradesh',NULL,NULL,'user.png','rahul@123'),(22,'2018-07-21 23:22:30','Piyush','Maheshwari','piyush.maheshwari_cs16@gla.ac.in','8273301369',NULL,'',NULL,NULL,'Mathura','gla university mathura','Uttar Pradesh',NULL,NULL,'user.png','piyush@123'),(23,'2018-07-21 23:30:13','Parth','Pathak','parth.pathak_cs16@gla.ac.in','9690221181',NULL,'',NULL,NULL,'Mathura','gla university mathura','Uttar Pradesh',NULL,NULL,'user.png','parth@123'),(24,'2018-07-23 02:52:47','Jharna ','AGRAWAL','Jharna.agrawal_cs16@gla.ac.in','8979608796',NULL,'',NULL,NULL,'Mathura','kosi kalan marhura','Uttar Pradesh',NULL,NULL,'user.png','jharna'),(25,'2018-07-23 04:55:55','Gaurang','Sharma','gaurang.sharma_cs16@gla.ac.in','9456437065',NULL,'',NULL,NULL,'Mathura','Krishna Nagar','Uttar Pradesh',NULL,NULL,'user.png','gaurang@123');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_entity_like`
--

DROP TABLE IF EXISTS `user_entity_like`;
CREATE TABLE `user_entity_like` (
  `ID` int(11) unsigned NOT NULL auto_increment,
  `user_id` int(11) unsigned default NULL,
  `user_entity_upload_id` int(11) unsigned default NULL,
  `CREATION_TIMESTAMP` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`ID`),
  UNIQUE KEY `unique_like` (`user_id`,`user_entity_upload_id`),
  KEY `fk14` (`user_entity_upload_id`),
  CONSTRAINT `fk13` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `fk14` FOREIGN KEY (`user_entity_upload_id`) REFERENCES `user_entity_upload` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_entity_like`
--

LOCK TABLES `user_entity_like` WRITE;
/*!40000 ALTER TABLE `user_entity_like` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_entity_like` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_entity_upload`
--

DROP TABLE IF EXISTS `user_entity_upload`;
CREATE TABLE `user_entity_upload` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `creation_timestamp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `entity_type_id` int(11) unsigned default NULL,
  `user_id` int(11) unsigned default NULL,
  `event_id` int(11) unsigned default NULL,
  `entity_file_url` varchar(127) NOT NULL,
  `verified` tinyint(1) default '0',
  PRIMARY KEY  (`id`),
  KEY `fk10` (`entity_type_id`),
  KEY `fk11` (`event_id`),
  KEY `fk12` (`user_id`),
  CONSTRAINT `fk10` FOREIGN KEY (`entity_type_id`) REFERENCES `entity_type` (`id`),
  CONSTRAINT `fk11` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`),
  CONSTRAINT `fk12` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_entity_upload`
--

LOCK TABLES `user_entity_upload` WRITE;
/*!40000 ALTER TABLE `user_entity_upload` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_entity_upload` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_role`
--

DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `creation_timestamp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `role_id` int(11) unsigned default NULL,
  `user_id` int(11) unsigned default NULL,
  PRIMARY KEY  (`id`),
  KEY `fk1` (`role_id`),
  KEY `fk2` (`user_id`),
  CONSTRAINT `fk1` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`),
  CONSTRAINT `fk2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_role`
--

LOCK TABLES `user_role` WRITE;
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
INSERT INTO `user_role` VALUES (1,'2018-06-21 00:57:46',2,1),(2,'2018-06-21 00:57:46',2,2),(3,'2018-06-21 00:57:46',2,3),(5,'2018-06-21 00:57:46',2,4),(6,'2018-06-21 00:57:26',1,5),(7,'2018-06-21 00:56:01',1,6),(8,'2018-06-21 00:56:05',1,7),(9,'2018-06-21 00:56:08',1,8),(10,'2018-06-21 00:56:12',1,9),(11,'2018-06-21 00:56:16',1,10);
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vendor_profile`
--

DROP TABLE IF EXISTS `vendor_profile`;
CREATE TABLE `vendor_profile` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `creation_timestamp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `name` varchar(255) NOT NULL,
  `street` varchar(63) NOT NULL,
  `city` varchar(63) NOT NULL,
  `locality` varchar(31) NOT NULL,
  `domain_id` int(11) unsigned default NULL,
  `user_id` int(11) unsigned default NULL,
  `shopNo` varchar(15) NOT NULL,
  `gstn` varchar(15) default NULL,
  `img` varchar(255) default 'vendor-2.jpg',
  `state` varchar(63) default NULL,
  PRIMARY KEY  (`id`),
  KEY `fk3` (`domain_id`),
  KEY `fk4` (`user_id`),
  CONSTRAINT `fk3` FOREIGN KEY (`domain_id`) REFERENCES `firm_domain` (`id`),
  CONSTRAINT `fk4` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vendor_profile`
--

LOCK TABLES `vendor_profile` WRITE;
/*!40000 ALTER TABLE `vendor_profile` DISABLE KEYS */;
INSERT INTO `vendor_profile` VALUES (1,'2018-07-21 23:14:40','Marvel Studios','231','Mathura','township mathura',4,21,'12','123123123','vendor-2.jpg','Uttar Pradesh'),(2,'2018-07-21 23:23:57','Big Bazar','3212','Mathura','Pacific mall mathura',1,22,'122','786786','vendor-2.jpg','Uttar Pradesh'),(3,'2018-07-21 23:33:02','McDonald\'s','212','Mathura','township mathura',6,23,'432','654321','vendor-2.jpg','Uttar Pradesh'),(4,'2018-07-23 04:59:28','TheRajau','Radha Nagar','Mathura','krishna nagar',2,25,'11','123456789123456','vendor-2.jpg','Uttar Pradesh');
/*!40000 ALTER TABLE `vendor_profile` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-08-03 11:51:17
