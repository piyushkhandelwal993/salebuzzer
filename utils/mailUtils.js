var nodemailer=require('nodemailer');

module.exports={
    sendMail:function(receiver,subject,msg,msgType='text'){
    var transporter=nodemailer.createTransport({
        service:'gmail',
        auth:{
            user:'promozonic@gmail.com',
            pass:'promozonic@123'
        }
    });

    if(msgType==='html'){
        var mailOptions={
            from:'promozonic@gmail.com',
            to:receiver,
            subject:subject,
            html:msg
        };
    }
    else{
        var mailOptions={
            from:'promozonic@gmail.com',
            to:receiver,
            subject:subject,
            text:msg
        };

    }

    transporter.sendMail(mailOptions,function(err,info){
        if(err)
            console.log(err);
        else
            console.log(info);
    });
  }
};