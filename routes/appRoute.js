var Router= require('koa-router');
var bodyParser = require('koa-body')();

module.exports = function(app){
    var router = new Router();

    //Home Routes
    var homeCtrl = require('./../controllers/homeCtrl');
    router.get('/home', homeCtrl.showHomePage);
    router.get('/logout', homeCtrl.logout);
    //contest Routes
    var contestCtrl = require('./../controllers/contestCtrl');
    router.get('/contests/:status', contestCtrl.showContestsPage);
    router.get('/:slug/contest/:eid', contestCtrl.showContestDetailPage);
    router.post('/newContest',contestCtrl.showNewContestForm);
    router.post('/newContest/photo', contestCtrl.addNewPhotoContest);
    router.post('/newContest/spinwheel', contestCtrl.addNewSpinWheel);
    router.post('/livecontest', contestCtrl.contestuplode);
    router.post('/likecount', contestCtrl.likeCount);
    router.post('/:slug/closeContest/:eid', contestCtrl.closeContest);
    router.post('/verifyPhoto', contestCtrl.verifyPhoto);
    router.post('/createCouponPhotoContest',contestCtrl.createCoupons);
    //Vendor dashboard
    var dashboardCtrl=require('./../controllers/dashboardCtrl');
    router.get('/dashboard/:vid',dashboardCtrl.showDashboard);
    router.get('/dashboard/:vid/allparticipants',dashboardCtrl.showallparticipants);
    router.post('/dashboard/upload',dashboardCtrl.uploadPhoto);
    //User Profiles
    var userCtrl=require('./../controllers/userCtrl');
    router.get('/profile/:uid',userCtrl.showUserProfile);
    router.post('/login',userCtrl.login);
    router.post('/signup', userCtrl.signup);
    router.post('/registerShop', userCtrl.registerShop);
    router.post('/profile/upload',userCtrl.uploadPhoto);
    //spinwheel
    var WheelCtrl = require('./../controllers/WheelCtrl');
    router.get('/:slug/spinwheel/:cid', WheelCtrl.showSpinWheel);
    router.post('/:slug/spinwheel/:cid', WheelCtrl.insertWheeldata);
    

    return router.middleware();
};