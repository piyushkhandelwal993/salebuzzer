var Router= require('koa-router');
var bodyParser = require('koa-body')();

module.exports = function(app){
    var router = new Router();

    //signup and login Routes
    var apiCtrl = require('./../controllers/apiCtrl');
    router.get('/domain',apiCtrl.getdomain);
    router.get('/signup', apiCtrl.getstate);
    router.get('/city',apiCtrl.getcity);
    router.post('/email',apiCtrl.checkemail);
    router.post('/phone',apiCtrl.checkphone);
    router.post('/gstn',apiCtrl.checkgstn);
    router.post('/password',apiCtrl.checkpassword);
    router.post('/confirmpassword', apiCtrl.checkconfirmpassword);

    //router.post('/likecount',signupCtrl.likeCount);
    return router.middleware();
}